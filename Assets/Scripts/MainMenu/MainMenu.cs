using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class MainMenu : MonoBehaviour
{
    public GameObject loadingScreen; //loading screen panel
    public GameObject chooserScreen; //map choosing panel

    public GameObject controllerInfo; //message that will be displayed when there is not connected controller
    private IEnumerator controllerInfoMessage; //prevent from coroutine stacking (after clicking multiple times on the button)

    public TextMeshProUGUI playerOneScoreText; //reference for TEXT component
    public TextMeshProUGUI playerTwoScoreText; //reference for TEXT component

    private void Start()
    {
        SetScoreText();

        loadingScreen.SetActive(false);
        chooserScreen.SetActive(false);
        controllerInfo.SetActive(false);
    }

    public void GoToChooserScreen() //OnClick function for button
    {
        if(!GameManager.CheckController())
        {
            if(controllerInfoMessage != null)
            {
                StopCoroutine(controllerInfoMessage);
                controllerInfo.SetActive(false);
            }

            controllerInfoMessage = ControllerInfoMessage();
            StartCoroutine(controllerInfoMessage);
        }
        else
            chooserScreen.SetActive(true);
    }

    private IEnumerator ControllerInfoMessage()
    {
        controllerInfo.SetActive(true);
        yield return new WaitForSeconds(2f);
        controllerInfo.SetActive(false);
    }

    public void StartGame() //OnClick function for button
    {
        StartCoroutine(LoadScene());
    }

    private IEnumerator LoadScene()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(1);
        loadingScreen.SetActive(true);

        while(!operation.isDone)
        {
            //values from 0 to 0.9 are about loading a screen (from 0.9 to 1 it's activating a scene)
            float progress = Mathf.Clamp01(operation.progress / 0.9f);

            loadingScreen.transform.GetChild(0).GetComponent<Slider>().value = progress;

            yield return null;
        }
    }

    public void ResetScore() //OnClick function for button
    {
        PlayerPrefs.SetInt(GameManager.PLAYER_ONE_SCORE, 0);
        PlayerPrefs.SetInt(GameManager.PLAYER_TWO_SCORE, 0);
        SetScoreText();
    }

    private void SetScoreText() //set score text in main menu
    {
        playerOneScoreText.text = PlayerPrefs.GetInt(GameManager.PLAYER_ONE_SCORE).ToString();
        playerTwoScoreText.text = PlayerPrefs.GetInt(GameManager.PLAYER_TWO_SCORE).ToString();
    }

    public void ExitGame() //OnClick function for button
    {
        Application.Quit();
    }
}
