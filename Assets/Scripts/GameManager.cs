using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public const string PLAYER_ONE_SCORE = "playerOneScore"; //name of variable to store in PlayerPrefs
    public const string PLAYER_TWO_SCORE = "playerTwoScore"; //name of variable to store in PlayerPrefs

    public static AudioSource musicPlayer { get; private set; }

    //the game has started only when counting ended (if game has started, we can display menu panel (ESC) or start checking if controller is connected)
    public static bool hasGameStarted { get; private set; }
    public static bool isControllerConnected { get; private set; } //check if controller has been disconnected

    public GameObject pausePanel;
    public GameObject endGamePanel;
    public GameObject playerOneStartPanel;
    public GameObject playerTwoStartPanel;

    public static PlayerManager[] players;

    public GameObject maps;

    private void Awake()
    {
        Application.targetFrameRate = 60;

        hasGameStarted = false;
        isControllerConnected = true;

        musicPlayer = GameObject.Find("MusicPlayer").GetComponent<AudioSource>();
        musicPlayer.volume = 0.5f;

        players = FindObjectsOfType<PlayerManager>();

        endGamePanel.SetActive(false);
        playerOneStartPanel.SetActive(true);
        playerTwoStartPanel.SetActive(true);

        SetMap();
        StartCoroutine(BeginGame());
    }

    private void Update()
    {
        if (hasGameStarted && !CheckController() && isControllerConnected)
        {
            isControllerConnected = false;
            pausePanel.SetActive(true);
            pausePanel.GetComponent<Pause>().SetControllerInfo(true);
        }
        else if(hasGameStarted && CheckController() && !isControllerConnected)
        {
            isControllerConnected = true;
            pausePanel.SetActive(false);
            pausePanel.GetComponent<Pause>().SetControllerInfo(false);
        }
    }

    public static bool CheckController() //true if controller is connected, otherwise false
    {
        return (Input.GetJoystickNames().Length > 0) && (Input.GetJoystickNames()[0] != "");
    }

    private IEnumerator BeginGame()
    {
        SetPlayersMovement(false);
        StartCoroutine(MusicDelay());

        for (int i = 3; i > 0 ; i--)
        {
            SetStartPanel(i.ToString());
            yield return new WaitForSeconds(1f);
        }

        SetStartPanel("START!");
        yield return new WaitForSeconds(1f);

        musicPlayer.volume = 1f;
        hasGameStarted = true;

        playerOneStartPanel.SetActive(false);
        playerTwoStartPanel.SetActive(false);
        SetPlayersMovement(true);
    }

    private IEnumerator MusicDelay() //this specific audio must have a delay
    {
        yield return new WaitForSeconds(.85f);
        playerOneStartPanel.GetComponent<AudioSource>().Play();
    }

    private void SetStartPanel(string text)
    {
        playerOneStartPanel.GetComponentInChildren<Text>().text = text;
        playerTwoStartPanel.GetComponentInChildren<Text>().text = text;
    }

    private void SetPlayersMovement(bool state)
    {
        foreach (PlayerManager player in players)
            player.SetMovementScript(state);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(1, LoadSceneMode.Single);
        Time.timeScale = 1;
    }

    private void SetMap()
    {
        maps.transform.GetChild(MapSelect.currentSelectedMap).gameObject.SetActive(true);
    }

    public void PlayerDeath(string playerName)
    {
        musicPlayer.volume = 0.5f;

        PlayerManager[] playerManagers = FindObjectsOfType<PlayerManager>();

        if (playerManagers[0].transform.name == playerName)
        {
            playerManagers[0].SetEndPanel(true, "You died!");
            playerManagers[1].SetEndPanel(true, "You win!");

            playerManagers[1].IncrementScore();
        }
        else
        {
            playerManagers[0].SetEndPanel(true, "You win!");
            playerManagers[1].SetEndPanel(true, "You died!");

            playerManagers[0].IncrementScore();
        }

        playerManagers[0].UpdateScoreText();
        playerManagers[1].UpdateScoreText();

        endGamePanel.SetActive(true);

        Time.timeScale = 0;
    }
}
