﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/*
 * Script for head game object
 * This object receives any collision that may occur
 */

public class Head : MonoBehaviour, IDamageable
{
    private PlayerManager playerManager;
    private GameManager gameManager;

    [HideInInspector] public bool isInviolability = false;

    //if there is a collision damage (player has hit something), then turn on Inviolability for whole snake
    private bool onCollisionDamage = false;

    private IEnumerator damageBlink;
    private Material originalMaterial;
    private Material blinkMaterial;

    private float maxHealth = 200f;

    private float health;
    private float Health
    {
        get { return health; }
        set
        {
            if (health > value) //if player/head lost some amount of health
            {
                if (damageBlink != null)
                {
                    StopCoroutine(damageBlink);
                    GetComponent<SpriteRenderer>().material = originalMaterial;
                }
                damageBlink = DamageBlink();
                StartCoroutine(damageBlink);

                if (onCollisionDamage)
                {
                    StartCoroutine(playerManager.SnakeFlashing());
                    onCollisionDamage = false;
                }
            }

            health = value;
            healthPanel.transform.GetChild(0).GetChild(0).GetComponent<Image>().fillAmount = Health / maxHealth;

            if (health <= 0)
                gameManager.PlayerDeath(transform.root.name);
        }
    }

    public GameObject healthPanel;

    private void Awake()
    {
        playerManager = transform.root.GetComponent<PlayerManager>();
        gameManager = FindObjectOfType<GameManager>();

        originalMaterial = GetComponent<SpriteRenderer>().material;
        blinkMaterial = Resources.Load<Material>("Materials/DamageBlink");

        Health = maxHealth;
    }

    public void TurnOffInviolability()
    {
        isInviolability = false;
        GetComponent<SpriteRenderer>().color = Color.white;
        GetComponent<CircleCollider2D>().enabled = true;
    }

    //activate inviolability when player hit another player or an obstacle
    public IEnumerator Inviolability() //Inviolability - nietykalnosc
    {
        GetComponent<CircleCollider2D>().enabled = false;
        isInviolability = true;

        while (isInviolability) //sprite blinking effect for head
        {
            GetComponent<SpriteRenderer>().color = Color32.Lerp(new Color32(255, 255, 255, 255), new Color32(255, 255, 255, 128), Mathf.PingPong(Time.time, 1f));
            yield return null;
        }
    }

    private IEnumerator delayForHittingHead() //delay dealing damage to both snakes, because both snakes must detect collision
    {
        GetComponent<CircleCollider2D>().enabled = false; //prevent from detecting collisions while waiting
        yield return new WaitForSeconds(0.01f);
    }

    private IEnumerator DamageBlink() //head blink when receiving damage
    {
        GetComponent<SpriteRenderer>().material = blinkMaterial;
        yield return new WaitForSeconds(0.1f);
        GetComponent<SpriteRenderer>().material = originalMaterial;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.transform.root.GetComponent<PlayerManager>())
        {
            if (collision.GetComponent<Head>()) //delay dealing damage to both snakes, because both snakes must detect collision
                StartCoroutine(delayForHittingHead());

            onCollisionDamage = true;
            DealDamage(20f); //deal damage to snake's head - for both body hit and head hit deal damage to this snake's head
        }
        else if (collision.TryGetComponent(out BuildingResource obj)) //if we hit/collect resources on the map
        {
            if (PickableTilesManager.TileType.wood == obj.myType)
                playerManager.Wood += obj.GetResource();
            else if (PickableTilesManager.TileType.stone == obj.myType)
                playerManager.Stone += obj.GetResource();
            else
                playerManager.Bronze += obj.GetResource();
        }
        else if (collision.CompareTag("Boundary")) //end of the map
            gameManager.PlayerDeath(transform.root.name);
    }
    
    public void DealDamage(float dmg) => Health -= dmg;

    public void SetOnCollisionDamage() => onCollisionDamage = true;
}
