using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/*
 * Script for power ups that player can pick up
 * After picking a power up it will be collected and stored until you use it
 * Each used power up give a bad effect for your enemy (another player)
 */

public abstract class PowerUp : PickableTile
{
    public Sprite image; //sprite of this power up
    public new string name; //name of this power up
    public float durationTime; //duration time of this power up

    private GameObject opponent; //store player to whom apply this power up

    protected abstract IEnumerator Effect(GameObject enemy);

    public void StartEffect()
    {
        StartCoroutine(Effect(opponent));
        AddToBar(opponent.GetComponent<PlayerManager>().powerUpsBar);
    }

    protected GameObject GetOpponent(GameObject player) 
        => (player.GetComponent<PlayerManager>() == GameManager.players[0]) ? GameManager.players[1].gameObject : GameManager.players[0].gameObject;

    protected void DisableInteraction() //after picking power up disable it's collider and fade out sprite
    {
        GetComponent<CircleCollider2D>().enabled = false;
        StartCoroutine(FadeOut());
    }

    protected void AddToBar(GameObject powerUpsBar) //add power up to active power ups bar in UI
    {
        GameObject slot = null;

        for(int i = 0; i < powerUpsBar.transform.childCount; i++)
        {
            if(!powerUpsBar.transform.GetChild(i).gameObject.activeSelf) //find inactive slot
            {
                slot = powerUpsBar.transform.GetChild(i).gameObject;
                break;
            }
        }

        if (slot == null)
            throw new System.Exception("No empty slot was found!");

        slot.GetComponent<Image>().sprite = image;
        slot.SetActive(true);
        StartCoroutine(DurationTimeOnBar(slot.transform.GetChild(0).GetComponent<Image>(), slot));
    }

    protected IEnumerator DurationTimeOnBar(Image timeBar, GameObject slot)
    {
        float speed = 1 / durationTime; // v = s / t, where s is max fill amount which is 1, and t is a duration time of a power up

        while(timeBar.fillAmount > 0)
        {
            timeBar.fillAmount -= speed * Time.deltaTime;
            yield return null;
        }

        slot.GetComponent<Image>().sprite = null;
        timeBar.fillAmount = 1f;
        slot.SetActive(false);
    }

    protected void DestroyPowerUp() //after using power up and power up has ended, destroy this gameobject
    {
        StopAllCoroutines();
        Destroy(gameObject);
    }

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        /*
         * if we will want to make a different way of collecting power ups when we have an power up and we can pick another power up to override previous one
         * we must remember to DELETE gameObject of this previous power up
        */

        if(collision.gameObject.transform.root.TryGetComponent(out PlayerManager playerManager))
        {
            if(playerManager.AddToPowerUpSlot(this))
            {
                //after collecting a power up, create another random power up after some time
                resourceManager.CreateResource(PickableTilesManager.TileType.powerUp, PickableTilesManager.DEFAULT_POWERUP_TIME);
                DisableInteraction();
                opponent = GetOpponent(collision.gameObject.transform.root.gameObject);
            }
        }
    }

    protected override void DestroyThisGameObject()
    {

    }
}
