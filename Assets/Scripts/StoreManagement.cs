﻿using UnityEngine;

/*
 * Script for moving through the store, buying turrets and placing them
 */

public class StoreManagement : MonoBehaviour
{
    private int activeTurretPanel; //stores information about what turret is being bought
    private int ActiveTurretPanel
    {
        get { return activeTurretPanel; }
        set
        {
            if((value >= 0) && (value <= transform.childCount - 1))
            {
                transform.GetChild(activeTurretPanel).gameObject.SetActive(false);
                activeTurretPanel = value;
                transform.GetChild(activeTurretPanel).gameObject.SetActive(true);
            }
        }
    }

    private bool isBuying = false; //describes when we selected a turret and now searching for empty place to build it
    private Transform bodies; //reference to all body/tail parts
    private int bodyCount = 0; //stores current body/tail part index in bodies/tail

    private SpriteRenderer currentBody; //current selected body/tail part

    private PlayerManager playerManager; //reference to read controls type (keyboard or joystick) and snake resources

    private GameObject turretsHealthPanel; //panel where are all slots for turrets health

    private void Awake()
    {
        ActiveTurretPanel = 0;
        bodies = transform.root.GetChild(1).transform;
        currentBody = bodies.GetChild(bodyCount).GetComponent<SpriteRenderer>();

        playerManager = transform.root.GetComponent<PlayerManager>();

        turretsHealthPanel = transform.root.Find("HealthBarCanvas").Find("Panel").gameObject;
    }

    private void Update()
    {
        if (!isBuying) //execute this when we are not in buying mode (selecting turrets)
        {
            if ((Input.GetKeyDown(KeyCode.RightArrow) && playerManager.controls.Equals(PlayerManager.Controls.keyboard))
                || (Input.GetKeyDown(KeyCode.Joystick1Button5) && playerManager.controls.Equals(PlayerManager.Controls.joystick))) 
                ActiveTurretPanel++;
            else if ((Input.GetKeyDown(KeyCode.LeftArrow) && playerManager.controls.Equals(PlayerManager.Controls.keyboard))
                || (Input.GetKeyDown(KeyCode.Joystick1Button4) && playerManager.controls.Equals(PlayerManager.Controls.joystick))) 
                ActiveTurretPanel--;
            else if ((Input.GetKeyDown(KeyCode.Return) && playerManager.controls.Equals(PlayerManager.Controls.keyboard))
                || (Input.GetKeyDown(KeyCode.Joystick1Button9) && playerManager.controls.Equals(PlayerManager.Controls.joystick)))
            {
                isBuying = true;
                SelectPlace(0);
            }
        }
        else if (isBuying) //execute this when we are in buying mode (selecting place for turret)
        {
            if ((Input.GetKeyDown(KeyCode.RightArrow) && playerManager.controls.Equals(PlayerManager.Controls.keyboard))
                || (Input.GetKeyDown(KeyCode.Joystick1Button5) && playerManager.controls.Equals(PlayerManager.Controls.joystick))) 
                SelectPlace(1);
            else if ((Input.GetKeyDown(KeyCode.LeftArrow) && playerManager.controls.Equals(PlayerManager.Controls.keyboard))
                || (Input.GetKeyDown(KeyCode.Joystick1Button4) && playerManager.controls.Equals(PlayerManager.Controls.joystick))) 
                SelectPlace(-1);
            else if ((Input.GetKeyDown(KeyCode.Return) && playerManager.controls.Equals(PlayerManager.Controls.keyboard))
                || (Input.GetKeyDown(KeyCode.Joystick1Button9) && playerManager.controls.Equals(PlayerManager.Controls.joystick))) 
                CreateTurret();
        }
    }

    private void SelectPlace(int offset) //after selecting turret, we must select a tail part in which a turret will be placed
    {
        if((bodyCount + offset >= 0) && (bodyCount + offset <= bodies.childCount - 1)) //check if we are not getting beyond body/tail parts count (check if the next body/tail part exists)
        {
            currentBody.color = new Color32(255, 255, 255, 255); //turn on highlight, on previous body/tail part
            bodyCount += offset; //get next selected body/tail part
            currentBody = bodies.GetChild(bodyCount).GetComponent<SpriteRenderer>();
            currentBody.color = new Color32(255, 255, 255, 128); //highlight newly selected body/tail part
        }
    }

    private void CreateTurret()
    {
        if (!bodies.GetChild(bodyCount).GetChild(0).CompareTag("Turret")) //check if given body part has already a turret    //CHANGE THIS - ADD BOOL VARIABLE TO BODY hasTurret
        {
            GameObject turret = Resources.Load<GameObject>(transform.GetChild(ActiveTurretPanel).name);

            //set turret layer to detect collisions between other enemy turrets
            if (playerManager.controls.Equals(PlayerManager.Controls.keyboard)) //if player plays with keyboard then he is Player1
                turret.layer = 8; //this layer value has been set in project settings
            else if (playerManager.controls.Equals(PlayerManager.Controls.joystick)) //if player plays with joystick then he is Player2
                turret.layer = 9; //this layer value has been set in project settings

            Turret resourcesCost = turret.GetComponent<Turret>();
            if (Buy(resourcesCost.woodCost, resourcesCost.stoneCost, resourcesCost.bronzeCost))
            {
                GameObject newTurret = Instantiate(turret, bodies.GetChild(bodyCount).transform);
                newTurret.transform.SetAsFirstSibling(); //set as first because there is one child object called Point Light 2D
                newTurret.layer = transform.root.gameObject.layer;

                GameObject healthPanel;
                for (int i = 0; i < turretsHealthPanel.transform.childCount; i++)
                {
                    if (!turretsHealthPanel.transform.GetChild(i).gameObject.activeSelf) //found empty/ready slot for turret health
                    {
                        healthPanel = turretsHealthPanel.transform.GetChild(i).gameObject;
                        healthPanel.SetActive(true);
                        //slot order is counting from snake's head (first tail part with turret is also first slot with health and so on)
                        healthPanel.transform.SetSiblingIndex(currentBody.transform.GetSiblingIndex() + 1); //+1 because first elemnt is always health of snake's head
                        newTurret.GetComponent<Turret>().SetHealthPanel(healthPanel);
                        break;
                    }
                }
            }
            else playerManager.DisplayMessage("You need more resources!"); //Debug.Log("You need more resources !");
        }
        else playerManager.DisplayMessage("This place is occupied"); //Debug.Log("This place is occupied");
    }

    public bool Buy(int woodCost, int stoneCost, int bronzeCost) //check if player can buy a turret and if he can then take resources
    {
        if(playerManager.Wood >= woodCost && playerManager.Stone >= stoneCost && playerManager.Bronze >= bronzeCost)
        {
            playerManager.Wood -= woodCost;
            playerManager.Stone -= stoneCost;
            playerManager.Bronze -= bronzeCost;
            return true;
        }

        return false;
    }

    private void OnDisable()
    {
        currentBody.color = new Color32(255, 255, 255, 255);
        isBuying = false;
    }
}
