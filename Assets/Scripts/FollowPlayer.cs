﻿using UnityEngine;

/*
 * Script for camera object to follow a player
 */

public class FollowPlayer : MonoBehaviour
{
    private Transform player;
    private Vector3 newPos;

    private void Awake()
    {
        player = transform.root.gameObject.transform.GetChild(0).transform;
    }

    private void Update()
    {
        newPos = Vector3.Lerp(transform.position, player.position, 2f * Time.deltaTime);
        newPos.z = -10f;
        transform.position = newPos;
    }
}
