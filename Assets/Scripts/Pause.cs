using UnityEngine;
using UnityEngine.SceneManagement;

/*
 * Script for pause panel which can be opened by using "Esc" during the game
 */

public class Pause : MonoBehaviour
{
    private GameObject controllerMessage;

    private void Awake()
    {
        controllerMessage = transform.GetChild(transform.childCount - 1).Find("ControllerInfo").gameObject;
    }

    private void OnEnable()
    {
        GameManager.musicPlayer.volume = 0.5f;
        Time.timeScale = 0; //when game is paused, stop game time
    }

    private void OnDisable()
    {
        GameManager.musicPlayer.volume = 1f;
        Time.timeScale = 1;
    }

    public void SetControllerInfo(bool state)
    {
        controllerMessage.SetActive(state);
    }

    public void MainMenu() //OnClick function for button
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
        Time.timeScale = 1;
    }
}
