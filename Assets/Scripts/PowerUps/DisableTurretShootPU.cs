using System.Collections;
using UnityEngine;

public class DisableTurretShootPU : PowerUp
{
    protected override IEnumerator Effect(GameObject enemy)
    {
        PlayerManager enemyManager = enemy.GetComponent<PlayerManager>();

        enemyManager.DisableTurretShoot(true);
        yield return new WaitForSeconds(durationTime);
        enemyManager.DisableTurretShoot(false);

        DestroyPowerUp();
    }
}
