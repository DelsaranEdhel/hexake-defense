using UnityEngine;

/*
 * Building resource is a resource that is used for building turrets
 */

public class BuildingResource : PickableTile
{
    public int quantity; //how many, this resource type will give to the player
    public Sprite[] sprites; //e.g wood with quantity of 10 units have 3 different sprites

    public PickableTilesManager.TileType myType; //set in inspector

    protected override void Awake()
    {
        if (sprites.Length > 1)
            GetComponent<SpriteRenderer>().sprite = sprites[Random.Range(0, sprites.Length)];

        base.Awake();
    }

    public int GetResource() //executed when player pick up this object
    {
        resourceManager.CreateResource(myType); //after collecting, create another resource of that type
        StartCoroutine(FadeOut());

        return quantity;
    }
}
